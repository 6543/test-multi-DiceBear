module test

go 1.18

require (
	codeberg.org/Codeberg/avatars v1.0.0
	github.com/fogleman/gg v1.3.0
	github.com/lafriks/go-svg v0.3.2
)

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/net v0.0.0-20211118161319-6a13c67c3ce4 // indirect
	golang.org/x/text v0.3.6 // indirect
)
