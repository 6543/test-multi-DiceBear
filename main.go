package main

import (
	"image"
	"image/draw"
	"image/png"
	"os"
	"strings"
	"time"

	"codeberg.org/Codeberg/avatars"
	"github.com/fogleman/gg"
	"github.com/lafriks/go-svg"
	"github.com/lafriks/go-svg/renderer"
	rendr_gg "github.com/lafriks/go-svg/renderer/gg"
)

func main() {
	size := 189

	space := size / 20

	img := image.NewRGBA(image.Rect(0, 0, size*2+space, size*2+space))

	for i := 0; i < 4; i++ {
		av, err := randomImageSize(size, []byte(time.Now().Format(time.RFC3339Nano)))
		if err != nil {
			panic(err)
		}
		pos := image.Rect((i-int(i/2)*2)*(size+space), int(i/2)*(size+space), ((i-int(i/2)*2)+1)*(size+space), (int(i/2)+1)*(size+space))
		draw.Draw(img, pos, av, image.Point{}, draw.Over)
	}

	f, err := os.Create("sample.png")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	png.Encode(f, img)
}

func randomImageSize(size int, data []byte) (image.Image, error) {
	svgAvatar := avatars.MakeAvatar(string(data))

	s, err := svg.Parse(strings.NewReader(svgAvatar), svg.IgnoreErrorMode)
	if err != nil {
		return nil, err
	}

	gc := gg.NewContext(size, size)
	rendr_gg.Draw(gc, s, renderer.Target(0, 0, float64(size), float64(size)))

	return gc.Image(), nil
}
